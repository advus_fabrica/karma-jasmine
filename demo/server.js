var express = require('express');
var app = express(); 
var port = process.env.PORT || 1230; 


app.use(express.static(__dirname + '/app')); 

 
require('./router.js')(app);


app.listen(port);
console.log("App listening on port " + port);