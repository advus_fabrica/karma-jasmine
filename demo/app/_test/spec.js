var Teams = require('./pageObjects.js');

describe('Testa a rota com os objetos da pagina', function() {
    
  var teams = new Teams.TeamsListPage();  

  it('Deve mostrar a lista de times na primeira pagina', function() {
    
    teams.open();

    expect(teams.getTeamsListRows().count()).toEqual(5);

    expect(teams.getRankForRow(0).getText())
      .toEqual('1');
    expect(teams.getNameForRow(0).getText())
      .toEqual('Spain');

    expect(teams.getRankForRow(4).getText())
      .toEqual('5');
    expect(teams.getNameForRow(4).getText())
      .toEqual('Uruguay');
 
    expect(teams.isLoginLinkVisible()).toBe(true);
    expect(teams.isLogoutLinkVisible()).toBe(false);
    
  });
  
  it('Deve permitir logar', function() {
        
    teams.openLogin();
    
    teams.getCtrlUser(); 
    
    teams.getCtrlPassword();
    
    teams.clickSubmit();
    
    expect(teams.sureRedirectUser())
        .toEqual('http://localhost:1230/#/');   
    
    expect(teams.isLoginLinkVisible()).toBe(false);
    expect(teams.isLogoutLinkVisible()).toBe(true);
    
  });

});


