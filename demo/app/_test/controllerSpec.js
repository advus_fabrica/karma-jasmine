describe('Server App Integration', function() {
  
  beforeEach(module('fifaApp'));

  var ctrl; 
  var mockBackend;
  
  beforeEach(inject(function($controller, $httpBackend) {
    
    mockBackend = $httpBackend;
    
   /* mockBackend.expectGET('/api/team')
               .respond(404, { msg: 'Not Found' }); */
      mockBackend.expectGET('/api/team').respond([
        { code:'ESP', name:'Spain' },
        { code: 'GER', name: 'Germany' },
        { code: 'POR', name: 'Portugal' },
        { code: 'COL', name: 'Colombia' },
        { code: 'URU', name: 'Uruguay' }
      ]);          
    
    ctrl = $controller('TeamListCtrl');            
      
  }));
    
  
  it('deve tratar os erros enquanto os itens são carregados', function() {
    
    //expect(ctrl.teams).toEqual([]);
    
    mockBackend.flush();
    
    //expect(ctrl.teams).toEqual([]);
    
    expect(ctrl.teams).toEqual([
      { code:'ESP', name:'Spain' },
      { code: 'GER', name: 'Germany' },
      { code: 'POR', name: 'Portugal' },
      { code: 'COL', name: 'Colombia' },
      { code: 'URU', name: 'Uruguay' }
    ]);
    
    //expect(ctrl.errorMessage).toEqual('Not Found');
    
  }); 

  afterEach(function() {
    
    mockBackend.verifyNoOutstandingExpectation();
    
    mockBackend.verifyNoOutstandingRequest();
  });

});