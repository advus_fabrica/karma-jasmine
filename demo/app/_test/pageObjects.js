exports.TeamsListPage = function () {
    
    this.open = function() {
        browser.get('/');
    };

    this.getTeamsListRows = function() {
        return element.all(by.repeater('team in teamListCtrl.teams'));
    };

    this.getRankForRow = function(row) {
        return element(
        by.repeater('team in teamListCtrl.teams')
            .row(row).column('team.rank'));
    };

    this.getNameForRow = function(row) {
        return element(
        by.repeater('team in teamListCtrl.teams')
            .row(row).column('team.name'));
    };

    this.isLoginLinkVisible = function() {
        return element(by.css('.login-link')).isDisplayed();
    };

    this.isLogoutLinkVisible = function() {
        return element(by.css('.logout-link')).isDisplayed();
    };
    
    this.openLogin  = function() {
        browser.get('#/login');
    }
    
    this.getCtrlUser = function() {
        var username = element(by.model('loginCtrl.user.username'));
        username.sendKeys('admin');       
    }
    
    this.getCtrlPassword = function() {
       var password = element(by.model('loginCtrl.user.password'));
       password.sendKeys('admin');    
    }          
    
    this.clickSubmit = function () {
        element(by.css('.btn.btn-success')).click();    
    }
    
    this.sureRedirectUser = function() {
        return browser.getCurrentUrl();
    }
    
    
}
