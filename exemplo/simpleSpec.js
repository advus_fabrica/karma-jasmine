// A Test Suite in Jasmine
describe('My Function', function() {

  var t;
  // Similar to setup
  beforeEach(function() {
    t = true;
  });

  afterEach(function() {
    t = null;
  });

  it('executar a ação 1 e espera que seja verdadeiro', function() {
    expect(t).toBeTruthy();
  });
  it('executar a ação 2 e espera que seja verdadeiro', function() {
    var expectedValue = true;
    expect(t).toEqual(expectedValue);
  });
});
